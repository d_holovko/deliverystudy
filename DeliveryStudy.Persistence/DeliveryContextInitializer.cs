﻿using Bogus;
using DeliveryStudy.Domain.Entities;

namespace DeliveryStudy.Persistence
{
    public class DeliveryContextInitializer
    {
        public static void Seed(DeliveryContext context)
        {
            // Db seed data
            var fakeOptions = new Faker<Option>()
                .RuleFor(x => x.Title, f => f.Lorem.Word())
                .RuleFor(x => x.Value, f => f.Lorem.Word());

            var fakeCategories = new Faker<Category>()
                .RuleFor(x => x.Title, f => f.Lorem.Word());

            var categories = fakeCategories.Generate(10);

            var fakeProducts = new Faker<Product>()
                .RuleFor(x => x.Title, f => f.Lorem.Word())
                .RuleFor(x => x.Category, f => f.PickRandom(categories));

            var fakeSellers = new Faker<Seller>()
                .RuleFor(x => x.Title, f => f.Lorem.Word())
                .RuleFor(x => x.Products, f => fakeProducts.Generate(f.Random.Int(3, 5)));

            var sellers = fakeSellers.Generate(5);

            context.Set<Category>().AddRange(categories);
            context.Set<Seller>().AddRange(sellers);

            context.SaveChanges();
        }
    }
}
