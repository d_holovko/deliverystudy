﻿using DeliveryStudy.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DeliveryStudy.Persistence
{
    public class DeliveryContext : DbContext
    {
        public virtual DbSet<Product> Products { get; set; }

        public DeliveryContext()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=ProductDb;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Ids settings
            modelBuilder.Entity<Product>().ToTable("Products");
            modelBuilder.Entity<Option>().ToTable("Options");
            modelBuilder.Entity<Seller>().ToTable("Sellers");
            modelBuilder.Entity<Order>().ToTable("Orders");
            modelBuilder.Entity<OrderDetail>().ToTable("OrderDetails");

            modelBuilder.Entity<Option>().HasKey(x => x.Id);
            modelBuilder.Entity<Product>().HasKey(x => x.Id);
            modelBuilder.Entity<Seller>().HasKey(x => x.Id);
            modelBuilder.Entity<Order>().HasKey(x => x.Id);
            modelBuilder.Entity<OrderDetail>().HasKey(x => new { x.OderId, x.ProductId });

            modelBuilder.Entity<Option>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Product>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Seller>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Order>().Property(x => x.Id).ValueGeneratedOnAdd();

            // Entities' relationships
            modelBuilder.Entity<Product>()
                .HasMany(x => x.Options)
                .WithOne(x => x.Product)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Seller>()
                .HasMany(x => x.Products)
                .WithOne(x => x.Shop)
                .HasForeignKey(x => x.ShopId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Order>()
                .HasMany(x => x.OrderDetails)
                .WithOne(x => x.Order)
                .HasForeignKey(x => x.OderId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Product>()
                .HasMany(x => x.OrderDetails)
                .WithOne(x => x.Product)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);
                
            base.OnModelCreating(modelBuilder);
        }
    }
}
