﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using DeliveryStudy.Domain.Entities;
using DeliveryStudy.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryStudy.Web.Controllers
{
    [ApiController]
    public class DeliveryController : ControllerBase
    {
        private DeliveryContext context;

        public DeliveryController(DeliveryContext deliveryContext)
        {
            context = deliveryContext;

            DeliveryContextInitializer.Seed(context);
        }

        [HttpGet]
        [Route("v1/products")]
        public ActionResult<IEnumerable<object>> Get()
        {
            var products = context.Products.Select(x => new {
                x.Id,
                x.Title,
                x.CategoryId,
                x.Options,
                x.ShopId
            }).ToList();

            return products;
        }

        [HttpGet]
        [Route("v1/products/{id:Guid}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        [HttpPost]
        [Route("v1/products")]
        public void Post([FromBody] string value)
        {
        }

        [HttpPut]
        [Route("v1/products/{id:Guid}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete]
        [Route("v1/products/{id:Guid}")]
        public void Delete(int id)
        {
        }
    }
}
