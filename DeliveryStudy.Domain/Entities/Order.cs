﻿using System;
using System.Collections.Generic;

namespace DeliveryStudy.Domain.Entities
{
    public class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public Guid Id { get; set; }
        public DateTime OrderDate { get; set; }
        public string DeliveryAddress { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
