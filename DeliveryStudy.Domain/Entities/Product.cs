﻿using System;
using System.Collections.Generic;

namespace DeliveryStudy.Domain.Entities
{
    public class Product
    {
        public Product()
        {
            Options = new HashSet<Option>();

            OrderDetails = new HashSet<OrderDetail>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public ICollection<Option> Options { get; private set; }

        public Seller Shop { get; set; }
        public Guid ShopId { get; set; }

        public Category Category { get; set; }
        public Guid CategoryId { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; private set; }
    }
}
