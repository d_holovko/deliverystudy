﻿using System;

namespace DeliveryStudy.Domain.Entities
{
    public class Option
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }

        public Product Product { get; set; }
        public Guid ProductId { get; set; }
    }
}
