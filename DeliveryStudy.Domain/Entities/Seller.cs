﻿using System;
using System.Collections.Generic;

namespace DeliveryStudy.Domain.Entities
{
    public class Seller
    {
        public Seller()
        {
            Products = new HashSet<Product>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
